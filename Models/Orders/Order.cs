﻿using System.Collections.Generic;

namespace Models.Orders
{
    public class Order
    {
        public string Phone { get; set; }

        public int DistrictId { get; set; }

        public List<int> SelectedProducts { get; set; }
    }
}