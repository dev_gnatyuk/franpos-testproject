﻿using System.Threading.Tasks;
using Server.Database;
using Server.Entities;
using Server.Repositories.Base;

namespace Server.Repositories.UnitOfWork
{
    public class UnitOfWork
    {
        private readonly ApplicationDbContext _applicationDbContext;

        public UnitOfWork(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
            Orders = new BaseRepository<Order>(applicationDbContext);
            Products = new BaseRepository<Product>(applicationDbContext);
            Districts = new BaseRepository<District>(applicationDbContext);
            
        }

        
        public IBaseRepository<Order> Orders { get; set; }
        public IBaseRepository<Product> Products { get; set; }
        public IBaseRepository<District> Districts { get; set; }

        public async Task CommitAsync()
        {
            await _applicationDbContext.SaveChangesAsync();
        }

        public ApplicationDbContext GetContext()
        {
            return _applicationDbContext;
        }
    }
}