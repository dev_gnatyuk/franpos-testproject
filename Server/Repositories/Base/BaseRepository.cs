﻿using System.Collections.Generic;
using System.Linq;
using Server.Database;

namespace Server.Repositories.Base
{

    public class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        private readonly ApplicationDbContext _dbContext;

        public BaseRepository(ApplicationDbContext context)
        {
            _dbContext = context;
        }

        public T Add(T entity)
        {
           return _dbContext.Set<T>().Add(entity);
        }

        public void AddRange(IEnumerable<T> array)
        {
            _dbContext.Set<T>().AddRange(array);
        }

        public bool Any()
        {
            return _dbContext.Set<T>().Any();
        }

        public bool Delete(T entity)
        {
            return _dbContext.Set<T>().Remove(entity) != null;
        }

        public IQueryable<T> Get()
        {
            return _dbContext.Set<T>();
        }
    }
}