﻿using System.Collections.Generic;
using System.Linq;

namespace Server.Repositories.Base
{
    public interface IBaseRepository<T> where T : class
    {
        T Add(T entity);
        void AddRange(IEnumerable<T> array);
        IQueryable<T> Get();
        bool Any();
        bool Delete(T entity);
    }
}