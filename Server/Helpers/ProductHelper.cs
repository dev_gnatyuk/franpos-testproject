﻿using AutoMapper;
using Server.Entities;
using Server.Models;

namespace Server.Helpers
{
    public static class ProductHelper
    {
        
        public static ProductViewModel ConvertToViewModel(Product product)
        {
            return Mapper.Map<Product, ProductViewModel>(product);
        }
        
    }
}