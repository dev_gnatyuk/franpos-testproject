﻿using AutoMapper;
using Server.Entities;
using Server.Models;

namespace Server.Helpers
{
    public static class OrderHelper
    {
        public static OrderViewModel ConvertToViewModel(Order order)
        {
            return Mapper.Map<Order, OrderViewModel>(order);
        }

        public static Order ConvertToEntity(OrderCreateViewModel create)
        {
            return Mapper.Map<OrderCreateViewModel, Order>(create);
        }
    }
}