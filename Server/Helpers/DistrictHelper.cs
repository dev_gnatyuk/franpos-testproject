﻿using AutoMapper;
using Server.Entities;
using Server.Models;

namespace Server.Helpers
{
    public static class DistrictHelper
    {
       
        public static DistrictViewModel ConvertToViewModel(District district)
        {
            return Mapper.Map<District, DistrictViewModel>(district);
        }
        
    }
}