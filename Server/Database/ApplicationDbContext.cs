﻿using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using Server.Entities;
using Server.Models;

namespace Server.Database
{

    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext()
            : base("DefaultConnection")
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<District> Districts { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Product> Products { get; set; }
    }
    
}