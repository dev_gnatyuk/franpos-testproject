﻿using System.Threading.Tasks;
using Server.Repositories.UnitOfWork;

namespace Server.Services.Base
{
    public class BaseService : IBaseService
    {
        protected readonly UnitOfWork _unitOfWork;
        public BaseService(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task CommitAsync()
        {
            await _unitOfWork.CommitAsync();
        }
    }
}