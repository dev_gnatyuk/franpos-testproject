﻿using System.Threading.Tasks;

namespace Server.Services.Base
{
    public interface IBaseService
    {
        Task CommitAsync();
    }
}