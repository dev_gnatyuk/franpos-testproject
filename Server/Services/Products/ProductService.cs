﻿using System.Collections.Generic;
using System.Linq;
using Server.Helpers;
using Server.Models;
using Server.Repositories.UnitOfWork;
using Server.Services.Base;
using Server.Services.Orders;

namespace Server.Services.Products
{
    public class ProductService : BaseService, IProductService
    {
        public ProductService(UnitOfWork unitOfWork) : base(unitOfWork)
        {

        }

        public List<ProductViewModel> Get()
        {
            var orders = _unitOfWork.Products.Get().ToList();
            return orders.Select(ProductHelper.ConvertToViewModel).ToList();
        }
    }
}