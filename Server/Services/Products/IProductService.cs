﻿using System.Collections.Generic;
using Server.Models;
using Server.Services.Base;

namespace Server.Services.Products
{
    public interface IProductService : IBaseService
    {
        List<ProductViewModel> Get();
    }
}