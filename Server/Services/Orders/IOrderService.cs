﻿using System.Collections.Generic;
using Server.Entities;
using Server.Models;
using Server.Services.Base;

namespace Server.Services.Orders
{
    public interface IOrderService : IBaseService
    {
        Order Create(Order model);
        Order Update(int orderId, string userId);
        List<OrderViewModel> GetNew(int district);
        List<OrderViewModel> Get(string userName);
    }
}