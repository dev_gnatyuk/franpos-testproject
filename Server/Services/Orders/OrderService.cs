﻿using System.Collections.Generic;
using System.Linq;
using Server.Entities;
using Server.Helpers;
using Server.Models;
using Server.Repositories.UnitOfWork;
using Server.Services.Base;

namespace Server.Services.Orders
{
    public class OrderService : BaseService, IOrderService
    {
        public OrderService(UnitOfWork unitOfWork) : base(unitOfWork)
        {

        }

        public Order Create(Order model)
        {
            var order = _unitOfWork.Orders.Add(new Order()
            {
                DistrictId = model.DistrictId,
                Phone = model.Phone,
                //SelectedProducts = new List<SelectedProduct>(model.SelectedProducts.Select(x=> new SelectedProduct(){ ProductId = x}))
            });

            return order;
        }

        public Order Update(int orderId, string userName)
        {
            var order = _unitOfWork.Orders.Get().SingleOrDefault(x => x.Id == orderId);
            if (order != null)
            {
                order.AdminName = userName;
            }
            return order;
        }

        public List<OrderViewModel> GetNew(int district)
        {
            return _unitOfWork.Orders.Get().Where(x => x.AdminName == null && x.DistrictId == district).AsEnumerable().Select(OrderHelper.ConvertToViewModel)
                .ToList();
        }

        public List<OrderViewModel> Get(string userName)
        {
            return _unitOfWork.Orders.Get().Where(x => x.AdminName == userName).AsEnumerable().Select(OrderHelper.ConvertToViewModel)
                .ToList();
        }
    }
}