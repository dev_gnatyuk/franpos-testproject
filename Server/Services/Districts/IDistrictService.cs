﻿using System.Collections.Generic;
using Server.Entities;
using Server.Models;
using Server.Services.Base;

namespace Server.Services.Districts
{
    public interface IDistrictService : IBaseService
    {
        List<DistrictViewModel> Get();
    }
}