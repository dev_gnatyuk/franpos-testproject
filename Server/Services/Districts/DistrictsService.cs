﻿using System.Collections.Generic;
using System.Linq;
using Server.Helpers;
using Server.Models;
using Server.Repositories.UnitOfWork;
using Server.Services.Base;

namespace Server.Services.Districts
{
    public class DistrictService : BaseService, IDistrictService
    {
        public DistrictService(UnitOfWork unitOfWork) : base(unitOfWork)
        {

        }

        public List<DistrictViewModel> Get()
        {
            return _unitOfWork.Districts.Get().ToList().Select(DistrictHelper.ConvertToViewModel).ToList();
        }
        
    }
}