﻿using System.Collections.Generic;
using Server.Services.Base;

namespace Server.Services.Users
{
    public interface IUserService : IBaseService
    {
        List<string> GetNamesByDistrictId(int districtId);
        List<string> GetNames();
    }
}