﻿using System.Collections.Generic;
using Server.Entities;

namespace Server.Models
{
    public class OrderCreateViewModel
    { 
        public string Phone { get; set; }

        public int DistrictId { get; set; }

        public List<int> SelectedProducts { get; set; }
    }

    public class OrderViewModel
    {
        public int Id { get; set; }
        public string Phone { get; set; }

        public int DistrictId { get; set; }
    }
}