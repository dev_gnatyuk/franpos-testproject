﻿using System.Collections.Generic;
using Server.Entities;

namespace Server.Models
{
    public class DistrictViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}