using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Server.Entities;

namespace Server.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Server.Database.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Server.Database.ApplicationDbContext context)
        {
            if (!context.Districts.Any())
            {
                context.Districts.AddRange(new List<District>()
                {
                    new District() {Name = "District 1"},
                    new District() {Name = "District 2"},
                    new District() {Name = "District 3"},
                });
                context.SaveChanges();
            }
        }
    }
}
