﻿namespace Server.Entities
{
    public class SelectedProduct
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
    }
}