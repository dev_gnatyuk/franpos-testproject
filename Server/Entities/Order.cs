﻿using System.Collections.Generic;
using Server.Models;

namespace Server.Entities
{
    public class Order
    {
        public int Id { get; set; }
        public string Phone { get; set; }

        public string AdminName { get; set; }

        public int DistrictId { get; set; }
        public District District { get; set; }

        public List<SelectedProduct> SelectedProducts { get; set; }
    }
}