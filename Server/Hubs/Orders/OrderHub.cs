﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.SignalR;
using Server.Database;
using Server.Entities;
using Server.Helpers;
using Server.Models;
using Server.Repositories.UnitOfWork;
using Server.Services.Orders;
using Server.Services.Users;

namespace Server.Hubs.Orders
{
    public class User {
        public string UserName { get; set; }
        public string ConnectedId { get; set; }
        public int District { get; set; }
    }
    public static class UserHandler
    {
        public static HashSet<User> Users = new HashSet<User>();
    }

    public class OrderHub : Hub
    {
        private readonly IOrderService _orderService;
        private readonly IUserService _userService;

        public OrderHub()
        {
            _orderService = new OrderService(new UnitOfWork(new ApplicationDbContext()));
            _userService = new UserService(new UnitOfWork(new ApplicationDbContext()));
        }

        public override Task OnConnected()
        {
            var district = Context.QueryString.Get("district");
            var userName = Context.QueryString.Get("userName");
            if (!string.IsNullOrEmpty(district))
            {
                Groups.Add(Context.ConnectionId, district);
                UserHandler.Users.Add(new User(){ConnectedId = Context.ConnectionId, District = Convert.ToInt32(district), UserName = userName });
            }

            return base.OnConnected();
        }

        public override Task OnDisconnected(bool a)
        {
            UserHandler.Users.RemoveWhere(x=>x.ConnectedId == Context.ConnectionId);
            return base.OnDisconnected(a);
        }

        public async Task Create(Order model)
        {
            var order = _orderService.Create(model);
            await _orderService.CommitAsync();

            Clients.Group(Convert.ToString(model.DistrictId)).created(OrderHelper.ConvertToViewModel(order));
        }

        public async Task Update(int orderId)
        {
            var user = UserHandler.Users.SingleOrDefault(x => x.ConnectedId == Context.ConnectionId);
            _orderService.Update(orderId, user?.UserName);
            await _orderService.CommitAsync();
            var clients = UserHandler.Users.Where(x => x.ConnectedId != Context.ConnectionId).Select(x=>x.ConnectedId).ToList();
            Clients.Clients(clients).removed(orderId);
        }
        
    }
}