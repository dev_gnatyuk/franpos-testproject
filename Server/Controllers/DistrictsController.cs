﻿using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using Server.Database;
using Server.Models;
using Server.Repositories.UnitOfWork;
using Server.Services.Districts;
using Server.Services.Orders;

namespace Server.Controllers
{
    [AllowAnonymous]
    [EnableCors(origins: "http://localhost:17470", headers: "*", methods: "*")]
    public class DistrictsController : ApiController
    {
        
        private readonly IDistrictService _districtService;

        public DistrictsController()
        {
            _districtService = new DistrictService(new UnitOfWork(new ApplicationDbContext()));
        }

        [HttpGet]
        public IHttpActionResult Get()
        {
            return Ok(_districtService.Get());
        }

    }
}
