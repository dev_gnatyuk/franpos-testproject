﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Server.Database;
using Server.Models;
using Server.Repositories.UnitOfWork;
using Server.Services.Orders;
using Server.Services.Products;

namespace Server.Controllers
{
    [AllowAnonymous]
    public class OrdersController : ApiController
    {
        private readonly IOrderService _orderService;

        public OrdersController()
        {
            _orderService = new OrderService(new UnitOfWork(new ApplicationDbContext()));
        }

        [HttpGet]
        public IHttpActionResult Get(string userName, int district)
        {
            var model = new ManageViewModel
            {
                Orders = _orderService.Get(userName),
                NewOrders = _orderService.GetNew(district)
            };

            return Ok(model);
        }
    }
}
