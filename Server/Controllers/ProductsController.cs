﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Server.Database;
using Server.Repositories.UnitOfWork;
using Server.Services.Products;

namespace Server.Controllers
{
    [AllowAnonymous]
    [EnableCors(origins: "http://localhost:17470", headers: "*", methods: "*")]
    public class ProductsController : ApiController
    {
        private readonly IProductService _productService;

        public ProductsController()
        {
            _productService = new ProductService(new UnitOfWork(new ApplicationDbContext()));
        }

        [HttpGet]
        public IHttpActionResult Get()
        {
            return Ok(_productService.Get());
        }
    }
}
