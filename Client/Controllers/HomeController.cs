﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.SignalR.Client;
using Models.Orders;

namespace Client.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task Create(Order order)
        {
            var hubConnection = new HubConnection("http://localhost:16212/signalr", useDefaultUrl: false);
            IHubProxy orderHub = hubConnection.CreateHubProxy("orderHub");

            await hubConnection.Start();

            await orderHub.Invoke("create", order);
        }

    }
}